package com.example.appmenubutton

import java.io.Serializable
data class AlumnoLista(
    var id: Int,
    var matricula: String = "",
    var nombre: String = "",
    val domicilio: String = "",
    var especialidad: String,
    var foto: String


): Serializable

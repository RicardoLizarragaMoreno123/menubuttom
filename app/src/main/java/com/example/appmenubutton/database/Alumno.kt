package com.example.appmenubutton.database

import java.io.Serializable

data class Alumno(
    val id: Int = 0,
    val matricula: String,
    val nombre: String,
    val domicilio: String,
    val especialidad: String,
    val foto: String
) : Serializable

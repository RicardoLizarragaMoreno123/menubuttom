package com.example.appmenubutton.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class AlumnosDbHelperer(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "sistema.db"
        private const val DATABASE_VERSION = 2
    }

    init {
        val dbFile = context.getDatabasePath(DATABASE_NAME)
        println("Database path: ${dbFile.absolutePath}")
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("""
            CREATE TABLE ${DefinirTabla.Alumnos.TABLA} (
                ${DefinirTabla.Alumnos.ID} INTEGER PRIMARY KEY AUTOINCREMENT,
                ${DefinirTabla.Alumnos.MATRICULA} TEXT,
                ${DefinirTabla.Alumnos.NOMBRE} TEXT,
                ${DefinirTabla.Alumnos.DOMICILIO} TEXT,
                ${DefinirTabla.Alumnos.ESPECIALIDAD} TEXT,
                ${DefinirTabla.Alumnos.FOTO} TEXT
            )
        """)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS ${DefinirTabla.Alumnos.TABLA}")
        onCreate(db)
    }
}

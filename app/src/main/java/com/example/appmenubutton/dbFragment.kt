package com.example.appmenubutton

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.appmenubutton.database.Alumno
import com.example.appmenubutton.database.dbAlumnos
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class dbFragment : Fragment() {

    private lateinit var btnGuardar: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnBorrar: Button
    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtEspecialidad: EditText
    private lateinit var txtUrlImagen: TextView
    private lateinit var imgAlumno: ImageView
    private lateinit var db: dbAlumnos

    private val IMAGE_PICK_CODE = 1000

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        btnGuardar = view.findViewById(R.id.btnGuardar)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnBorrar = view.findViewById(R.id.btnBorrar)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        txtUrlImagen = view.findViewById(R.id.txturl)
        imgAlumno = view.findViewById(R.id.imgAlumno)

        try {
            if (isAdded) {
                db = dbAlumnos(requireContext())
            } else {
                throw IllegalStateException("Fragment not attached to a context.")
            }
        } catch (e: Exception) {
            Log.e("dbFragment", "Error initializing database: ${e.message}", e)
            Toast.makeText(requireContext(), "Error initializing database", Toast.LENGTH_SHORT).show()
            return null
        }

        arguments?.let {
            val alumnoLista = it.getSerializable("mialumno") as? AlumnoLista
            alumnoLista?.let { alumno ->
                txtNombre.setText(alumno.nombre)
                txtDomicilio.setText(alumno.domicilio)
                txtEspecialidad.setText(alumno.especialidad)
                txtMatricula.setText(alumno.matricula)
                txtUrlImagen.setText(alumno.foto)
                loadImage(alumno.foto)
            }
        }

        imgAlumno.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, IMAGE_PICK_CODE)
        }

        btnGuardar.setOnClickListener {
            if (valiEmpty()) {
                Toast.makeText(requireContext(), "Por favor, ingrese toda la información", Toast.LENGTH_SHORT).show()
            } else {
                val fotoPath = txtUrlImagen.text.toString()
                val alumno = Alumno(
                    nombre = txtNombre.text.toString(),
                    matricula = txtMatricula.text.toString(),
                    domicilio = txtDomicilio.text.toString(),
                    especialidad = txtEspecialidad.text.toString(),
                    foto = fotoPath
                )

                try {
                    val existingAlumno = db.BuscarAlumno(alumno.matricula)

                    if (existingAlumno != null) {
                        val result = db.ActualizarAlumno(alumno)
                        if (result > 0) {
                            Toast.makeText(requireContext(), "Alumno actualizado con éxito", Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(requireContext(), "Error al actualizar el alumno", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        val id: Long = db.InsertarAlumno(alumno)
                        if (id > 0) {
                            Toast.makeText(requireContext(), "Alumno agregado con éxito con ID $id", Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(requireContext(), "Error al agregar el alumno", Toast.LENGTH_SHORT).show()
                        }
                    }
                    limpiar()
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), "Error al acceder a la base de datos", Toast.LENGTH_SHORT).show()
                    Log.e("dbFragment", "Database access error: ${e.message}", e)
                }
            }
        }

        btnBuscar.setOnClickListener {
            val matricula = txtMatricula.text.toString()
            if (matricula.isEmpty()) {
                Toast.makeText(requireContext(), "Por favor, ingrese la matrícula para buscar", Toast.LENGTH_SHORT).show()
            } else {
                try {
                    val alumno = db.BuscarAlumno(matricula)
                    if (alumno != null) {
                        txtNombre.setText(alumno.nombre)
                        txtDomicilio.setText(alumno.domicilio)
                        txtEspecialidad.setText(alumno.especialidad)
                        txtUrlImagen.setText(alumno.foto)
                        loadImage(alumno.foto)
                        Toast.makeText(requireContext(), "Alumno encontrado", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireContext(), "La matrícula ingresada no existe", Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), "Error al acceder a la base de datos", Toast.LENGTH_SHORT).show()
                    Log.e("dbFragment", "Database access error: ${e.message}", e)
                }
            }
        }

        btnBorrar.setOnClickListener {
            val matricula = txtMatricula.text.toString()
            if (matricula.isEmpty()) {
                Toast.makeText(requireContext(), "Por favor, ingrese la matrícula para borrar", Toast.LENGTH_SHORT).show()
            } else {
                val builder = AlertDialog.Builder(requireContext())
                builder.setMessage("¿Está seguro de que desea eliminar este alumno?")
                    .setPositiveButton("Sí") { dialog, id ->
                        try {
                            val result = db.BorrarAlumno(matricula)
                            if (result > 0) {
                                Toast.makeText(requireContext(), "Alumno eliminado con éxito", Toast.LENGTH_SHORT).show()
                                limpiar()
                            } else {
                                Toast.makeText(requireContext(), "La matrícula ingresada no existe", Toast.LENGTH_SHORT).show()
                            }
                        } catch (e: Exception) {
                            Toast.makeText(requireContext(), "Error al acceder a la base de datos", Toast.LENGTH_SHORT).show()
                            Log.e("dbFragment", "Database access error: ${e.message}", e)
                        }
                    }
                    .setNegativeButton("No") { dialog, id ->
                        dialog.dismiss()
                    }
                builder.create().show()
            }
        }

        return view
    }

    private fun valiEmpty(): Boolean {
        return txtNombre.text.toString().isEmpty() ||
                txtDomicilio.text.toString().isEmpty() ||
                txtMatricula.text.toString().isEmpty() ||
                txtEspecialidad.text.toString().isEmpty() ||
                txtUrlImagen.text.toString().isEmpty()
    }

    private fun limpiar() {
        txtMatricula.text.clear()
        txtNombre.text.clear()
        txtDomicilio.text.clear()
        txtEspecialidad.text.clear()
        txtUrlImagen.setText("")
        imgAlumno.setImageResource(0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            val imageUri = data?.data
            imageUri?.let {
                try {
                    val savedImagePath = saveImageToInternalStorage(it)
                    imgAlumno.setImageURI(it)
                    txtUrlImagen.setText(savedImagePath)
                } catch (e: IOException) {
                    Toast.makeText(requireContext(), "Error al guardar la imagen", Toast.LENGTH_SHORT).show()
                    Log.e("dbFragment", "Image save error: ${e.message}", e)
                }
            }
        }
    }

    private fun saveImageToInternalStorage(uri: Uri): String {
        val inputStream = requireContext().contentResolver.openInputStream(uri)
        val file = File(requireContext().filesDir, "${System.currentTimeMillis()}.jpg")
        val outputStream = FileOutputStream(file)
        inputStream.use { input ->
            outputStream.use { output ->
                input?.copyTo(output)
            }
        }
        return file.absolutePath
    }

    private fun loadImage(imagePath: String?) {
        if (imagePath != null) {
            val imageFile = File(imagePath)
            if (imageFile.exists()) {
                imgAlumno.setImageURI(Uri.fromFile(imageFile))
            } else {
                imgAlumno.setImageResource(R.mipmap.ic_launcher)
            }
        } else {
            imgAlumno.setImageResource(R.mipmap.ic_launcher)
        }
    }
}

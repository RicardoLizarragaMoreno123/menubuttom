package com.example.appmenubutton

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appmenubutton.database.dbAlumnos
import com.google.android.material.floatingactionbutton.FloatingActionButton


class SalirFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var miAdaptador: MiAdaptador
    private lateinit var listaAlumnos: List<AlumnoLista>
    private lateinit var db: dbAlumnos

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            // Puedes usar los parámetros si los necesitas
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflar el layout para este fragmento
        val view = inflater.inflate(R.layout.fragment_salir, container, false)

        recyclerView = view.findViewById(R.id.recId)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        // Inicializar base de datos y cargar datos
        db = dbAlumnos(requireContext())
        db.openDataBase()
        listaAlumnos = db.leerTodos()
        db.close()

        miAdaptador = MiAdaptador(requireContext(), listaAlumnos) { alumno ->
            cambiarDBFragment(alumno)
        }
        recyclerView.adapter = miAdaptador

        val agregarAlumno = view.findViewById<FloatingActionButton>(R.id.agregarAlumno)
        agregarAlumno.setOnClickListener {
            cambiarDBFragment(null)
        }

        return view
    }

    private fun cambiarDBFragment(alumno: AlumnoLista?) {
        val dbFragment = dbFragment().apply {
            arguments = Bundle().apply {
                putSerializable("mialumno", alumno)
            }
        }
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.frmContenedor, dbFragment)
            .addToBackStack(null)
            .commit()
    }



    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SalirFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
